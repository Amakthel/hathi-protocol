*2019-03-01:*  
*We, the Hathi authors,  are continuing this project and claim copyright to the changes that we will be making henceforth.*  
*We will be continuing to publish under the Apache open source license.*  
*2019-02-28:*  
*As our sponsoring organisation has decided to spin down, it has been agreed to public-domain the Hathi project.*  
*Code created on or before this date is free for everyone to use and/or claim.*  

# hathi-protocol

Go implementation of the hathi protocol.

The protocol data itself is handled in the packet.go file. Protocol usage is
in endpoint.go.

Has support for golang's new module system. This repo may be built from a
location that is not under $GOPATH so long as golang-1.11 or greater is used.
# Using the client-mocker to test hathi-server

The server isn't here, it's over in hathi-social/hathi-server. However, the client-mocker,
for testing, is here. To use it, you first have to build it. This is simple,
just use

```
make build
```
in the same directory as this file, and it should compile things for you.

To test the server, you first need a running server. The README.md in
hathi-social/hathi-server will tell you how to get it running.

Once that is running, and you've bulit the client-mocker, run

```
./client-mocker /path/to/server/project/hathi-server/tests/name-of-test-data.hathitest
```

And you should get several lines back, that look something like the following:

```
Running sequence: /path/to/server/project/hathi-server/tests/name-of-test-data.hathitest
name-of-test-data.hathitest... 1/3
name-of-test-data.hathitest... 2/3
name-of-test-data.hathitest... 3/3
All sequences successful.
```

The tests that the client-mocker runs aren't with the client-mocker, they're
with the server in hathi-social/hathi-server/tests. If you want to make new
tests, put them there, not here with the mocker.

One common problem is forgetting to delete the server databas between tests.
Each time you run a test, shutdown the server, delete hathi.db (it should be
in the same directory as the server itself), restart the server, and only then
can you run your next test.

If you forget, you'll see an error on the very first test, typically one
indicating that it didn't get the packet it expected to get.
