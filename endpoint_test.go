package protocol

// Copyright 2018-present Hathi authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import (
	"bufio"
	"encoding/json"
	"io"
	"testing"
	"time"
	"reflect"
)

// Utility code

type fakeStream struct {
	inbound  string
	outbound string
}

func (f *fakeStream) Read(p []byte) (n int, err error) {
	reqLen := len(p)
	if reqLen == 0 {
		return 0, nil
	}
	// nothing in inbound
	if len(f.inbound) == 0 {
		return 0, io.EOF
	}
	inLen := len(f.inbound)
	if inLen < reqLen {
		// not enough in inbound
		for i, v := range []byte(f.inbound) {
			p[i] = v
		}
		f.inbound = ""
		return inLen, nil
	} else {
		// enough in inbound
		for i, _ := range p {
			p[i] = f.inbound[i]
		}
		f.inbound = f.inbound[reqLen:]
		return reqLen, nil
	}
}

func (f *fakeStream) Write(p []byte) (n int, err error) {
	f.outbound += string(p)
	return len(p), nil
}

// Tests

func TestStreamBuffer(t *testing.T) {
	sbuf := newStreamBuffer()
	sbuf.Append([]byte("foobarbaz"))
	sbuf.Append([]byte("qwerty"))
	// Get a partial input
	data := make([]byte, 5)
	rlen, err := sbuf.Read(data)
	if err != nil {
		t.Fatal("error:", err)
	}
	if rlen != 5 {
		t.Fatal("read length error:", rlen)
	}
	if reflect.DeepEqual(data, []byte{'f', 'o', 'o', 'b', 'a'}) != true {
		t.Fatal("data error:", data)
	}
}

func TestEndpointKernel(t *testing.T) {
	// ========== Setup ==========
	// create test data
	stream := fakeStream{inbound: `{"version":1,"function":"foo","request":5}{"version":1,"function":"ping","request":6}{"version":1,"function":"quux","request":7}`}
	pkt := Packet{Version: 1, RequestID: 2, Function: "asdf"}
	// create connection
	endpoint := NewEndpointKernel(&stream)
	// set features
	endpoint.SetImplementedFunctions([]string{"foo", "bar"})
	// ========== Begin test ==========
	// run the kernel
	endpoint.Run()
	time.Sleep(1 * time.Second) // Give the handler time to eat
	// At this point the fake inbound packets will have been processed and
	// in some cases responded to.
	// Send a packet
	endpoint.SendPacket(&pkt)
	// Get a packet. There should be only one available.
	fooPkt := endpoint.GetPacket()
	if fooPkt == nil {
		t.Fatal("fooPkt nil failure")
	} else if fooPkt.Version != 1 || fooPkt.Function != "foo" ||
		fooPkt.RequestID != 5 {
		t.Fatal("fooPkt incorrect:", fooPkt)
	}
	// Should be empty here
	quuxPkt := endpoint.GetPacket()
	if quuxPkt != nil {
		t.Fatal("quuxPkt not rejected as unimplemented")
	}
	// Stop the kernel
	endpoint.Stop()
	// ========== Check Output ==========
	// first convert strings to Packets
	stream.inbound = stream.outbound // Swap for Read()
	decoder := json.NewDecoder(bufio.NewReader(&stream))
	packets := []*Packet{}
	var err error
	for true {
		datum := make(map[string]interface{}, 0)
		err = decoder.Decode(&datum)
		if err != nil {
			break
		}
		newPacket, errParse := ParseMap(datum)
		if errParse != nil {
			t.Fatal("Packet parse error:", datum)
		}
		packets = append(packets, newPacket)
	}
	if err != io.EOF {
		t.Fatal("Packet conversion error:", err)
	}
	// Now check over the packets
	if len(packets) != 3 {
		t.Fatal("Incorrect sent packet count:", packets)
	}
	pongPkt := packets[0] // Should be a reply to the ping
	if pongPkt.Version != 1 || pongPkt.Function != "pong" ||
		pongPkt.RequestID != 6 {
		t.Fatal("pong packet failure:", pongPkt)
	}
	noimpPkt := packets[1] // Should be a reply to the 'quux'
	if noimpPkt.Version != 1 || noimpPkt.Function != "not-implemented" ||
		noimpPkt.RequestID != 7 {
		t.Fatal("not implemented packet failure:", noimpPkt)
	}
	asdfPkt := packets[2] // Should be the test packet we sent
	if asdfPkt.Version != 1 || asdfPkt.Function != "asdf" ||
		asdfPkt.RequestID != 2 {
		t.Fatal("asdf packet failure:", asdfPkt)
	}
}

func TestEndpointKernelClose(t *testing.T) {
	// ========== Setup ==========
	// create test data
	stream := fakeStream{inbound: ""}
	// create connection
	endpoint := NewEndpointKernel(&stream)
	// ========== Begin test ==========
	// run the kernel
	endpoint.Run()
	time.Sleep(1 * time.Second) // Give the handler time to eat
	// Get a packet. There should be none available.
	pkt := endpoint.GetPacket()
	if pkt != nil {
		t.Fatal("EndpointKernel pkt not nil")
	}
	if endpoint.IsClosed() == false {
		t.Fatal("EndpointKernel still running")
	}
}
